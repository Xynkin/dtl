#pragma once

constexpr unsigned long long operator"" _Hz(unsigned long long x) {
  return x;
}
constexpr unsigned long long operator"" _KHz(unsigned long long x) {
  return x*1000u;
}
constexpr unsigned long long operator"" _MHz(unsigned long long x) {
  return x*1'000'000u;
}

constexpr unsigned long long operator"" _MB(unsigned long long x) {
  return x*1024u*1024u;
}
constexpr unsigned long long operator"" _KB(unsigned long long x) {
  return x*1024u;
}
