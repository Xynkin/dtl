#pragma once

constexpr unsigned long long operator"" _bit(unsigned long long n) {
  return 1<<n;
}

constexpr unsigned bit(unsigned n) {
  return 1<<n;
}
constexpr unsigned bit(unsigned n, unsigned x) {
  return x<<n;
}

constexpr unsigned bits(unsigned msb, unsigned lsb, unsigned x) {
  return (x&((1u<<(msb - lsb + 1u)) - 1u))<<lsb;
}
