#pragma once

namespace dtl {

    template<typename ...Args>
    constexpr auto sum(Args... args) {
        return (args + ...);
    }

} // namespace dtl
