#pragma once

namespace dtl {

    template<unsigned _Address, unsigned _Offset>
    struct _offset {
        static constexpr const unsigned ADDRESS { _Address + _Offset };
        template<unsigned Offset> using offset = _offset<ADDRESS, Offset>;
    };
    template<unsigned Address>
    struct address {
        static constexpr const unsigned ADDRESS { Address };
        template<unsigned Offset> using offset = _offset<ADDRESS, Offset>;
    };

    template<typename Address, typename Type>
    struct iomem {
        static volatile Type& ref() noexcept {
            return *reinterpret_cast<volatile Type*>(Address::ADDRESS);
        }

        struct range {
            static volatile Type& ref(unsigned index) noexcept {
                return *(reinterpret_cast<volatile Type*>(Address::ADDRESS) + index);
            }
        };
    };

} // namespace dtl
