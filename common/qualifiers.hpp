#pragma once

namespace dtl {

    template<typename T>
    constexpr T& remove_volatile(volatile T& val) {
        return const_cast<T&>(val);
    }

} // namespace dtl
