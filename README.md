# dtl

A C++ template library, mostly for embedded systems. Aims to create a versatile set of highly reusable and configurable abstractions in form of templates.