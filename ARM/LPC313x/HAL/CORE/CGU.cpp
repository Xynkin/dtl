#include "dtl/ARM/LPC313x/HAL/REG/CGU.h"

using dtl::CGU;

namespace {

    enum class src_clock_t: unsigned {
        FFAST_12MHZ,
        I2SRX_BCK0,
        I2SRX_WS0,
        I2SRX_BCK1,
        I2SRX_WS1,
        HPPLL0,
        HPPLL1
    };

    enum class domain_t: unsigned {
        begin,
        SYS = begin,
        AHB_APB0,
        AHB_APB1,
        AHB_APB2,
        AHB_APB3,
        PCM,
        UART,
        CLK1024FS,
        I2SRX_BCK0,
        I2SRX_BCK1,
        SPI_CLK,
        SYSCLK_O,
        end
    };

    enum class switch_t: unsigned {
        SIDE1,
        SIDE2
    };

    enum class clock_t: unsigned {
        begin,
        /* SYS */
        APB0_CLK = begin, APB1_CLK, APB2_CLK, APB3_CLK, APB4_CLK,
        AHB_TO_INTC_CLK, AHB0_CLK, EBI_CLK, DMA_PCLK, DMA_CLK_GATED,
        NANDFLASH_S0_CLK, NANDFLASH_ECC_CLK, NANDFLASH_NAND_CLK = 13,
        NANDFLASH_PCLK, CLOCK_OUT, ARM926_CORE_CLK, ARM926_BUSIF_CLK,
        ARM926_RETIME_CLK, SD_MMC_HCLK, SD_MMC_CCLK_IN, USB_OTG_AHB_CLK,
        ISRAM0_CLK, RED_CTL_RSCLK, ISRAM1_CLK, ISROM_CLK, MPMC_CFG_CLK,
        MPMC_CFG_CLK2, MPMC_CFG_CLK3, INTC_CLK,
        /* AHB_APB0 */
        AHB_TO_APB0_PCLK, EVENT_ROUTER_PCLK, ADC_PCLK, ADC_CLK,
        WDOG_PCLK, IOCONF_PCLK, CGU_PCLK, SYSCREG_PCLK, RNG_PCLK = 39,
        /* AHB_APB1 */
        AHB_TO_APB1_PCLK, TIMER0_PCLK, TIMER1_PCLK, TIMER2_PCLK,
        TIMER3_PCLK, PWM_PCLK, PWM_PCLK_REGS, PWM_CLK, I2C0_PCLK,
        I2C1_PCLK,
        /* AHB_APB2 */
        AHB_TO_APB2_PCLK, PCM_PCLK, PCM_APB_PCLK, UART_APB_CLK, LCD_PCLK,
        LCD_CLK, SPI_PCLK, SPI_PCLK_GATED,
        /* AHB_APB3 */
        AHB_TO_APB3_PCLK, I2S_CFG_PCLK, EDGE_DET_PCLK, I2STX_FIFO_0_PCLK,
        I2STX_IF_0_PCLK, I2STX_FIFO_1_PCLK, I2STX_IF_1_PCLK,
        I2SRX_FIFO_0_PCLK, I2SRX_IF_0_PCLK, I2SRX_FIFO_1_PCLK,
        I2SRX_IF_1_PCLK,
        /* PCM */
        PCM_CLK_IP = 71,
        /* UART */
        UART_U_CLK,
        /* CLK1024FS */
        I2S_EDGE_DETECT_CLK, I2STX_BCK0_N, I2STX_WS0, I2STX_CLK0,
        I2STX_BCK1_N, I2STX_WS1, CLK_256FS, I2SRX_BCK0_N, I2SRX_WS0,
        I2SRX_BCK1_N, I2SRX_WS1,
        /* I2SRX_BCK0 */
        I2SRX_BCK0 = 87,
        /* I2SRX_BCK1 */
        I2SRX_BCK1,
        /* SPI_CLK */
        SPI_CLK, SPI_CLK_GATED,
        /* SYSCLK_O */
        SYSCLK_O,
        end
    };

    enum class divider_t: unsigned {
        begin,
        /* SYS_BASE */
        FDIV0 = begin, FDIV1, FDIV2, FDIV3, FDIV4, FDIV5, FDIV6,
        /* AHB_APB0_BASE */
        FDIV7, FDIV8,
        /* AHB_APB1_BASE */
        FDIV9, FDIV10,
        /* AHB_APB2_BASE */
        FDIV11, FDIV12, FDIV13,
        /* AHB_APB3_BASE */
        FDIV14,
        /* PCM_BASE */
        FDIV15,
        /* UART_BASE */
        FDIV16,
        /* CLK1024FS_BASE */
        FDIV17, FDIV18, FDIV19, FDIV20, FDIV21, FDIV22,
        /* SPI_CLK_BASE */
        FDIV23,
        end
    };

    const unsigned PCR[] {
        /* SYS */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR0 APB0_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR1 APB1_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR2 APB2_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR3 APB3_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR4 APB4_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR5 AHB_TO_INTC_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR6 AHB0_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR7 EBI_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, // PCR8 DMA_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, // PCR9 DMA_CLK_GATED
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR10 NANDFLASH_S0_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR11 NANDFLASH_ECC_CLK
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR12 reserved
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR13 NANDFLASH_NAND_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR14 NANDFLASH_PCLK
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR15 CLOCK_OUT
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR16 ARM926_CORE_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 1 }, //PCR17 ARM926_BUSIF_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR18 ARM926_RETIME_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR19 SD_MMC_HCLK *75MHz max*
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR20 SD_MMC_CCLK_IN *25MHz for SD card*
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR21 USB_OTG_AHB_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR22 ISRAM0_CLK
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR23 RED_CTL_RSCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR24 ISRAM1_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR25 ISROM_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 1 }, //PCR26 MPMC_CFG_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR27 MPMC_CFG_CLK2
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR28 MPMC_CFG_CLK3
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR29 INTC_CLK
        /* AHB_APB0 */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR30 AHB_TO_APB0_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR31 EVENT_ROUTER_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR32 ADC_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR33 ADC_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR34 WDOG_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR35 IOCONF_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR36 CGU_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR37 SYSCREG_PCLK
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR38 reserved
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR39 RNG_PCLK
        /* AHB_APB1 */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR40 AHB_TO_APB1_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR41 TIMER0_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR42 TIMER1_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR43 TIMER2_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR44 TIMER3_PCLK
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR45 PWM_PCLK
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 1 }, //PCR46 PWM_PCLK_REGS
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR47 PWM_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR48 I2C0_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR49 I2C1_PCLK
        /* AHB_APB2 */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR50 AHB_TO_APB2_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR51 PCM_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR52 PCM_APB_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR53 UART_APB_CLK *10-50MHz*
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR54 LCD_PCLK
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR55 LCD_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR56 SPI_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR57 SPI_PCLK_GATED
        /* AHB_APB3 */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR58 AHB_TO_APB3_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR59 I2S_CFG_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR60 EDGE_DET_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR61 I2STX_FIFO_0_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR62 I2STX_IF_0_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR63 I2STX_FIFO_1_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR64 I2STX_IF_1_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR65 I2SRX_FIFO_0_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR66 I2SRX_IF_0_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR67 I2SRX_FIFO_1_PCLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR68 I2SRX_IF_1_PCLK
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR69 reserved
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR70 reserved
        /* PCM */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR71 PCM_CLK_IP
        /* UART */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR72 UART_U_CLK *10-50MHz*
        /* CLK1024FS */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR73 I2S_EDGE_DETECT_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR74 I2STX_BCK0_N
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR75 I2STX_WS0
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR76 I2STX_CLK0
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR77 I2STX_BCK1_N
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR78 I2STX_WS1
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR79 CLK_256FS
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR80 I2SRX_BCK0_N
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR81 I2SRX_WS0
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR82 I2SRX_BCK1_N
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR83 I2SRX_WS1
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR84 reserved
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR85 reserved
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR86 reserved
        /* I2SRX_BCK0 */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR87 I2SRX_BCK0
        /* I2SRX_BCK1 */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR88 I2SRX_BCK1
        /* SPI_CLK */
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 0, .ENOUT_EN = 0 }, //PCR89 SPI_CLK
        CGU::SW::pcr_t { .RUN = 1, .AUTO = 1, .WAKE_EN = 1, .EXTEN_EN = 1, .ENOUT_EN = 0 }, //PCR90 SPI_CLK_GATED
        /* SYSCLK_O */
        CGU::SW::pcr_t { .RUN = 0, .AUTO = 0, .WAKE_EN = 0, .EXTEN_EN = 0, .ENOUT_EN = 0 }  //PCR91 SYSCLK_O
    };

    const unsigned FDC[] {
        /* SYS */
        CGU::SW::fdc_t   { .RUN = 1, .STRETCH = 1 }.set(1, 2),   // FDC0
        CGU::SW::fdc_t   { .RUN = 1, .STRETCH = 1 }.set(1, 4),   // FDC1
        CGU::SW::fdc_t   { .RUN = 1, .STRETCH = 1 }.set(1, 8),   // FDC2
        CGU::SW::fdc_t   { .RUN = 0               },             // FDC3
        CGU::SW::fdc_t   { .RUN = 0               },             // FDC4
        CGU::SW::fdc_t   { .RUN = 0               },             // FDC5
        CGU::SW::fdc_t   { .RUN = 0               },             // FDC6
        /* AHB_APB0 */
        CGU::SW::fdc_t   { .RUN = 1, .STRETCH = 1 }.set(1, 128), // FDC7
        CGU::SW::fdc_t   { .RUN = 0               },             // FDC8
        /* AHB_APB1 */
        CGU::SW::fdc_t   { .RUN = 1, .STRETCH = 1 }.set(1, 12),  // FDC9
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC10
        /* AHB_APB2 */
        CGU::SW::fdc_t   { .RUN = 1, .STRETCH = 1 }.set(1, 2),   //FDC11
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC12
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC13
        /* AHB_APB3 */
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC14
        /* PCM */
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC15
        /* UART */
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC16
        /* CLK1024FS */
        CGU::SW::fdc17_t { .RUN = 1, .STRETCH = 1 }.set(1, 256), //FDC17 Fs = 44.1 KHz, Fout = 11.2896 MHz
        CGU::SW::fdc_t   { .RUN = 1, .STRETCH = 1 }.set(1, 4),   //FDC18 32 bit per channel, 2 channels
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC19
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC20
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC21
        CGU::SW::fdc_t   { .RUN = 0               },             //FDC22
        /* SPI_CLK */
        CGU::SW::fdc_t   { .RUN = 1, .STRETCH = 1 }.set(1, 2),   //FDC23
    };

    const unsigned ESR[] {
        /* SYS */
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR0 APB0_CLK           = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR1 APB1_CLK           = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR2 APB2_CLK           = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR3 APB3_CLK           = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR4 APB4_CLK           = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR5 AHB_TO_INTC_CLK    = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR6 AHB0_CLK           = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR7 EBI_CLK            = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR8 DMA_PCLK           = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, // ESR9 DMA_CLK_GATED      = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC1   }, //ESR10 NANDFLASH_S0_CLK   = 180/4 = 45 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC1   }, //ESR11 NANDFLASH_ECC_CLK  = 180/4 = 45 MHz
        CGU::SW::esr0to29_t  { .EN = 0                                            }, //ESR12 reserved
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC1   }, //ESR13 NANDFLASH_NAND_CLK = 180/4 = 45 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC1   }, //ESR14 NANDFLASH_PCLK     = 180/4 = 45 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC2   }, //ESR15 CLOCK_OUT          = 180/8 = 22,5 MHz
        CGU::SW::esr0to29_t  { .EN = 0                                            }, //ESR16 ARM926_CORE_CLK    = 180 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR17 ARM926_BUSIF_CLK   = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 0                                            }, //ESR18 ARM926_RETIME_CLK  = 180 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR19 SD_MMC_HCLK        = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC1   }, //ESR20 SD_MMC_CCLK_IN     = 180/4 = 45 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR21 USB_OTG_AHB_CLK    = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR22 ISRAM0_CLK         = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC1   }, //ESR23 RED_CTL_RSCLK      = 180/4 = 45 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR24 ISRAM1_CLK         = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR25 ISROM_CLK          = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR26 MPMC_CFG_CLK       = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR27 MPMC_CFG_CLK2      = 180/2 = 90 MHz
        CGU::SW::esr0to29_t  { .EN = 0                                            }, //ESR28 MPMC_CFG_CLK3      = 180 MHz
        CGU::SW::esr0to29_t  { .EN = 1, .SEL = CGU::SW::esr0to29_t::fdc_t::FDC0   }, //ESR29 INTC_CLK           = 180/2 = 90 MHz
        /* AHB_APB0 */
        CGU::SW::esr30to39_t { .EN = 0                                            }, //ESR30 AHB_TO_APB0_PCLK
        CGU::SW::esr30to39_t { .EN = 0                                            }, //ESR31 EVENT_ROUTER_PCLK
        CGU::SW::esr30to39_t { .EN = 0                                            }, //ESR32 ADC_PCLK
        CGU::SW::esr30to39_t { .EN = 1, .SEL = CGU::SW::esr30to39_t::fdc_t::FDC7  }, //ESR33 ADC_CLK
        CGU::SW::esr30to39_t { .EN = 0,                                           }, //ESR34 WDOG_PCLK
        CGU::SW::esr30to39_t { .EN = 0,                                           }, //ESR35 IOCONF_PCLK
        CGU::SW::esr30to39_t { .EN = 0,                                           }, //ESR36 CGU_PCLK
        CGU::SW::esr30to39_t { .EN = 0,                                           }, //ESR37 SYSCREG_PCLK
        CGU::SW::esr30to39_t { .EN = 0,                                           }, //ESR38 reserved
        CGU::SW::esr30to39_t { .EN = 0,                                           }, //ESR39 RNG_PCLK
        /* AHB_APB1 */
        CGU::SW::esr40to49_t { .EN = 1, .SEL = CGU::SW::esr40to49_t::fdc_t::FDC9  }, //ESR40 AHB_TO_APB1_PCLK
        CGU::SW::esr40to49_t { .EN = 1, .SEL = CGU::SW::esr40to49_t::fdc_t::FDC9  }, //ESR41 TIMER0_PCLK
        CGU::SW::esr40to49_t { .EN = 1, .SEL = CGU::SW::esr40to49_t::fdc_t::FDC9  }, //ESR42 TIMER1_PCLK
        CGU::SW::esr40to49_t { .EN = 1, .SEL = CGU::SW::esr40to49_t::fdc_t::FDC9  }, //ESR43 TIMER2_PCLK
        CGU::SW::esr40to49_t { .EN = 1, .SEL = CGU::SW::esr40to49_t::fdc_t::FDC9  }, //ESR44 TIMER3_PCLK
        CGU::SW::esr40to49_t { .EN = 0,                                           }, //ESR45 PWM_PCLK
        CGU::SW::esr40to49_t { .EN = 0,                                           }, //ESR46 PWM_PCLK_REGS
        CGU::SW::esr40to49_t { .EN = 0,                                           }, //ESR47 PWM_CLK
        CGU::SW::esr40to49_t { .EN = 0,                                           }, //ESR48 I2C0_PCLK
        CGU::SW::esr40to49_t { .EN = 0,                                           }, //ESR49 I2C1_PCLK
        /* AHB_APB2 */
        CGU::SW::esr50to57_t { .EN = 1, .SEL = CGU::SW::esr50to57_t::fdc_t::FDC11 }, //ESR50 AHB_TO_APB2_PCLK
        CGU::SW::esr50to57_t { .EN = 1, .SEL = CGU::SW::esr50to57_t::fdc_t::FDC11 }, //ESR51 PCM_PCLK
        CGU::SW::esr50to57_t { .EN = 1, .SEL = CGU::SW::esr50to57_t::fdc_t::FDC11 }, //ESR52 PCM_APB_PCLK
        CGU::SW::esr50to57_t { .EN = 1, .SEL = CGU::SW::esr50to57_t::fdc_t::FDC11 }, //ESR53 UART_APB_CLK
        CGU::SW::esr50to57_t { .EN = 1, .SEL = CGU::SW::esr50to57_t::fdc_t::FDC11 }, //ESR54 LCD_PCLK
        CGU::SW::esr50to57_t { .EN = 1, .SEL = CGU::SW::esr50to57_t::fdc_t::FDC11 }, //ESR55 LCD_CLK
        CGU::SW::esr50to57_t { .EN = 1, .SEL = CGU::SW::esr50to57_t::fdc_t::FDC11 }, //ESR56 SPI_PCLK
        CGU::SW::esr50to57_t { .EN = 1, .SEL = CGU::SW::esr50to57_t::fdc_t::FDC11 }, //ESR57 SPI_PCLK_GATED
        /* AHB_APB3 */
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR58 AHB_TO_APB3_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR59 I2S_CFG_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR60 EDGE_DET_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR61 I2STX_FIFO_0_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR62 I2STX_IF_0_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR63 I2STX_FIFO_1_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR64 I2STX_IF_1_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR65 I2SRX_FIFO_0_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR66 I2SRX_IF_0_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR67 I2SRX_FIFO_1_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR68 I2SRX_IF_1_PCLK
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR69 reserved
        CGU::SW::esr58to70_t { .EN = 0                                            }, //ESR70 reserved
        /* PCM */
        CGU::SW::esr71_t     { .EN = 0                                            }, //ESR71 PCM_CLK_IP
        /* UART */
        CGU::SW::esr72_t     { .EN = 0                                            }, //ESR72 UART_U_CLK
        /* CLK1024FS */
        CGU::SW::esr73to86_t { .EN = 1, .SEL = CGU::SW::esr73to86_t::fdc_t::FDC17 }, //ESR73 I2S_EDGE_DETECT_CLK
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR74 I2STX_BCK0_N
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR75 I2STX_WS0
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR76 I2STX_CLK0
        CGU::SW::esr73to86_t { .EN = 1, .SEL = CGU::SW::esr73to86_t::fdc_t::FDC18 }, //ESR77 I2STX_BCK1_N
        CGU::SW::esr73to86_t { .EN = 1, .SEL = CGU::SW::esr73to86_t::fdc_t::FDC17 }, //ESR78 I2STX_WS1
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR79 CLK_256FS
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR80 I2SRX_BCK0_N
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR81 I2SRX_WS0
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR82 I2SRX_BCK1_N
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR83 I2SRX_WS1
        CGU::SW::esr73to86_t { .EN = 0,                                           }, //ESR84 reserved
        /* I2SRX_BCK0 */
        0                                                                          , //ESR85 reserved
        /* I2SRX_BCK1 */
        0                                                                          , //ESR86 reserved
        /* SPI_CLK */
        CGU::SW::esr87to88_t { .EN = 1                                            }, //ESR87 SPI_CLK
        CGU::SW::esr87to88_t { .EN = 1                                            }  //ESR88 SPI_CLK_GATED
        /* SYSCLK_O */
    };

    void set(domain_t domain, switch_t side, src_clock_t clock) noexcept {
        if( side == switch_t::SIDE1 ){
            CGU::SW::FS1::ref(unsigned(domain)) = unsigned(clock);
            CGU::SW::SCR::ref(unsigned(domain)) = CGU::SW::scr_t { .ENF1 = 1 };
        }else{
            CGU::SW::FS2::ref(unsigned(domain)) = unsigned(clock);
            CGU::SW::SCR::ref(unsigned(domain)) = CGU::SW::scr_t { .ENF2 = 1 };
        }
    }

} // namespace

void initCGU() noexcept {
    for( auto domain = domain_t::begin; domain < domain_t::end; domain = domain_t(unsigned(domain) + 1u) ){
        set(domain, switch_t::SIDE1, src_clock_t::FFAST_12MHZ);
    }

    CGU::SW::BCR::ref(0) = CGU::SW::BCR::ref(1) = CGU::SW::BCR::ref(2) = CGU::SW::BCR::ref(3) = CGU::SW::BCR::ref(4) = 0u;

    for( auto i = 0u; i < 89u; ++i ){
        CGU::SW::ESR::ref(i) = 0u;
    }

    for( auto clock = clock_t::begin; clock < clock_t::end; clock = clock_t(unsigned(clock) + 1u) ){
        CGU::SW::PCR::ref(unsigned(clock)) = PCR[unsigned(clock)];
    }

    // Fout = 180 MHz
    CGU::CFG::HP1_MODE::ref() = CGU::CFG::hp_mode_t { .POWER_DOWN = 1 };
    CGU::CFG::HP1_FIN_SELECT::ref() = unsigned(src_clock_t::FFAST_12MHZ);
    CGU::CFG::HP1_MDEC::ref() = 8191u;
    CGU::CFG::HP1_NDEC::ref() = 770u;
    CGU::CFG::HP1_PDEC::ref() = 98u;
    CGU::CFG::HP1_SELR::ref() = 0u;
    CGU::CFG::HP1_SELI::ref() = 16u;
    CGU::CFG::HP1_SELP::ref() = 8u;
    CGU::CFG::HP1_MODE::ref() = CGU::CFG::hp_mode_t { .ENABLE = 1 };
    while( !CGU::CFG::HP1_STATUS::ref().LOCK ) ;

    // Fs = 44.1 KHz, Fout = 11.2896 MHz
    CGU::CFG::HP0_MODE::ref() = CGU::CFG::hp_mode_t { .POWER_DOWN = 1 };
    CGU::CFG::HP0_FIN_SELECT::ref() = unsigned(src_clock_t::FFAST_12MHZ);
    CGU::CFG::HP0_MDEC::ref() = 29784u;
    CGU::CFG::HP0_NDEC::ref() = 131u;
    CGU::CFG::HP0_PDEC::ref() = 7u;
    CGU::CFG::HP0_SELR::ref() = 0u;
    CGU::CFG::HP0_SELI::ref() = 8u;
    CGU::CFG::HP0_SELP::ref() = 31u;
    CGU::CFG::HP0_MODE::ref() = CGU::CFG::hp_mode_t { .ENABLE = 1 };
    while( !CGU::CFG::HP0_STATUS::ref().LOCK ) ;

    for( auto divider = divider_t::begin; divider < divider_t::end; divider = divider_t(unsigned(divider) + 1u) ){
        CGU::SW::FDC::ref(unsigned(divider)) = FDC[unsigned(divider)];
    }

    for( auto i = 0u; i < 89u; ++i ){
        CGU::SW::ESR::ref(i) = ESR[i];
    }

    CGU::SW::BCR::ref(0) = CGU::SW::BCR::ref(1) = CGU::SW::BCR::ref(2) = CGU::SW::BCR::ref(3) = CGU::SW::BCR::ref(4) = 1u;

    set(domain_t::SYS, switch_t::SIDE2, src_clock_t::HPPLL1);
    set(domain_t::AHB_APB2, switch_t::SIDE2, src_clock_t::HPPLL1);
    set(domain_t::SPI_CLK, switch_t::SIDE2, src_clock_t::HPPLL1);
    set(domain_t::CLK1024FS, switch_t::SIDE2, src_clock_t::HPPLL0);
}
