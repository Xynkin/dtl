#pragma once

#include "dtl/common/bit.h"
#include "dtl/ARM/LPC313x/HAL/REG/IOCONF.h"

namespace dtl {

    enum class GPIOs: unsigned {
        _1, _0, _2, _3, _4, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20
    };

    template<GPIOs N>
    struct GPIO {
        static constexpr const auto mask { bit(unsigned(N)) };

        static void make_output() noexcept {
            IOCONF::GPIO::MODE1_SET::ref() = mask;
        }
        static void set() noexcept {
            IOCONF::GPIO::MODE0_SET::ref() = mask;
        }
        static void clr() noexcept {
            IOCONF::GPIO::MODE0_CLR::ref() = mask;
        }

        static void make_input() noexcept {
            IOCONF::GPIO::MODE1_CLR::ref() = mask;
        }
        static bool get() noexcept {
            return IOCONF::GPIO::PIN::ref() & mask;
        }

        static void make_peripheral() noexcept {
            IOCONF::GPIO::MODE0_SET::ref() = mask;
            IOCONF::GPIO::MODE1_CLR::ref() = mask;
        }
    };

} // namespace dtl
