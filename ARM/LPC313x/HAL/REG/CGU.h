#pragma once

#include "dtl/common/iomem.h"

namespace dtl {

    struct CGU: address<0x1300'4000u> {

        struct SW: offset<0x000u> {
            struct scr_t {
                unsigned ENF1     :1;
                unsigned ENF2     :1;
                unsigned RESET    :1;
                unsigned STOP     :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };

            struct pcr_t {
                unsigned RUN      :1;
                unsigned AUTO     :1;
                unsigned WAKE_EN  :1;
                unsigned EXTEN_EN :1;
                unsigned ENOUT_EN :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };

            struct fdc_t {
                unsigned RUN      :1;
                unsigned RESET    :1;
                unsigned STRETCH  :1;
                unsigned MADD     :8;
                unsigned MSUB     :8;

                unsigned set(unsigned n, unsigned m) noexcept
                {
                    int madd = m - n;
                    int msub = -n;
                    for( ;; ){
                        auto tmadd = madd*2;
                        auto tmsub = msub*2;
                        if( tmadd < 256 && tmsub > -256 ){
                            madd = tmadd;
                            msub = tmsub;
                        }else break;
                    }
                    MADD = *reinterpret_cast<unsigned*>(&madd);
                    MSUB = *reinterpret_cast<unsigned*>(&msub);

                    return *reinterpret_cast<unsigned*>(this);
                }
                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct fdc17_t {
                unsigned RUN      :1;
                unsigned RESET    :1;
                unsigned STRETCH  :1;
                unsigned MADD     :13;
                unsigned MSUB     :13;

                unsigned set(unsigned n, unsigned m) noexcept
                {
                    int madd = m - n;
                    int msub = -n;
                    for( ;; ){
                        auto tmadd = madd*2;
                        auto tmsub = msub*2;
                        if( tmadd < 8192 && tmsub > -8192 ){
                            madd = tmadd;
                            msub = tmsub;
                        }else break;
                    }
                    MADD = *reinterpret_cast<unsigned*>(&madd);
                    MSUB = *reinterpret_cast<unsigned*>(&msub);

                    return *reinterpret_cast<unsigned*>(this);
                }
                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };

            struct esr0to29_t {
                // SYS
                enum fdc_t: unsigned { FDC0, FDC1, FDC2, FDC3, FDC4, FDC5, FDC6 };
                unsigned EN       :1;
                fdc_t    SEL      :3;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct esr30to39_t {
                // AHB0_APB0
                enum fdc_t: unsigned { FDC7, FDC8 };
                unsigned EN       :1;
                fdc_t    SEL      :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct esr40to49_t {
                // AHB0_APB1
                enum fdc_t: unsigned { FDC9, FDC10 };
                unsigned EN       :1;
                fdc_t    SEL      :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct esr50to57_t {
                // AHB0_APB2
                enum fdc_t: unsigned { FDC11, FDC12, FDC13 };
                unsigned EN       :1;
                fdc_t    SEL      :2;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct esr58to70_t {
                // AHB0_APB3
                enum fdc_t: unsigned { FDC14 };
                unsigned EN       :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct esr71_t {
                // PCM
                enum fdc_t: unsigned { FDC15 };
                unsigned EN       :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct esr72_t {
                // UART
                enum fdc_t: unsigned { FDC16 };
                unsigned EN       :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct esr73to86_t {
                // CLK1024FS
                enum fdc_t: unsigned { FDC17, FDC18, FDC19, FDC20, FDC21, FDC22 };
                unsigned EN       :1;
                fdc_t    SEL      :3;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct esr87to88_t {
                // SPI_CLK
                enum fdc_t: unsigned { FDC23 };
                unsigned EN       :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };

            using SCR                              = iomem<offset<0x000u>,       unsigned>::range;
            using FS1                              = iomem<offset<0x030u>,       unsigned>::range;
            using FS2                              = iomem<offset<0x060u>,       unsigned>::range;
            using SSR                              = iomem<offset<0x090u>, const unsigned>::range;
            using PCR                              = iomem<offset<0x0C0u>,       unsigned>::range;
            using PSR                              = iomem<offset<0x230u>, const unsigned>::range;
            using ESR                              = iomem<offset<0x3A0u>,       unsigned>::range;
            using BCR                              = iomem<offset<0x504u>,       unsigned>::range;
            using FDC                              = iomem<offset<0x518u>,       unsigned>::range;
            using DYN_FDC                          = iomem<offset<0x578u>,       unsigned>::range;
            using DYN_SEL                          = iomem<offset<0x594u>,       unsigned>::range;
        };

        struct CFG: offset<0xC00u> {
            struct hp_mode_t {
                unsigned ENABLE     :1;
                unsigned SKEW       :1;
                unsigned POWER_DOWN :1;
                unsigned DIRECTI    :1;
                unsigned DIRECTO    :1;
                unsigned FREE_RUN   :1;
                unsigned BANDSEL    :1;
                unsigned LIMUP_OFF  :1;
                unsigned BYPASS     :1;

                operator unsigned() noexcept { return *reinterpret_cast<unsigned*>(this); }
            };
            struct hp_status_t {
                unsigned LOCK       :1;
                unsigned FREE_RUN   :1;
            };

            using POWERMODE                        = iomem<offset<0x000u>,       unsigned>;
            using WD_BARK                          = iomem<offset<0x004u>, const unsigned>;
            using FFAST_ON                         = iomem<offset<0x008u>,       unsigned>;
            using FFAST_BYPASS                     = iomem<offset<0x00Cu>,       unsigned>;

            using APB0_RESETN_SOFT                 = iomem<offset<0x010u>,       unsigned>;
            using AHB_TO_APB0_PNRES_SOFT           = iomem<offset<0x014u>,       unsigned>;
            using APB1_RESETN_SOFT                 = iomem<offset<0x018u>,       unsigned>;
            using AHB_TO_APB1_PNRES_SOFT           = iomem<offset<0x01Cu>,       unsigned>;
            using APB2_RESETN_SOFT                 = iomem<offset<0x020u>,       unsigned>;
            using AHB_TO_APB2_PNRES_SOFT           = iomem<offset<0x024u>,       unsigned>;
            using APB3_RESETN_SOFT                 = iomem<offset<0x028u>,       unsigned>;
            using AHB_TO_APB3_PNRES_SOFT           = iomem<offset<0x02Cu>,       unsigned>;
            using APB4_RESETN_SOFT                 = iomem<offset<0x030u>,       unsigned>;
            using AHB_TO_INTC_RESETN_SOFT          = iomem<offset<0x034u>,       unsigned>;
            using AHB0_RESETN_SOFT                 = iomem<offset<0x038u>,       unsigned>;
            using EBI_RESET_N_SOFT                 = iomem<offset<0x03Cu>,       unsigned>;
            using PCM_PNRES_SOFT                   = iomem<offset<0x040u>,       unsigned>;
            using PCM_RESET_N_SOFT                 = iomem<offset<0x044u>,       unsigned>;
            using PCM_RESET_ASYNC_N_SOFT           = iomem<offset<0x048u>,       unsigned>;
            using TIMER0_PNRES_SOFT                = iomem<offset<0x04Cu>,       unsigned>;
            using TIMER1_PNRES_SOFT                = iomem<offset<0x050u>,       unsigned>;
            using TIMER2_PNRES_SOFT                = iomem<offset<0x054u>,       unsigned>;
            using TIMER3_PNRES_SOFT                = iomem<offset<0x058u>,       unsigned>;
            using ADC_PRESETN_SOFT                 = iomem<offset<0x05Cu>,       unsigned>;
            using ADC_RESETN_ADC10BITS_SOFT        = iomem<offset<0x060u>,       unsigned>;
            using PWM_RESET_AN_SOFT                = iomem<offset<0x064u>,       unsigned>;
            using UART_SYS_RST_AN_SOFT             = iomem<offset<0x068u>,       unsigned>;
            using I2C0_PNRES_SOFT                  = iomem<offset<0x06Cu>,       unsigned>;
            using I2C1_PNRES_SOFT                  = iomem<offset<0x070u>,       unsigned>;
            using I2S_CFG_RST_N_SOFT               = iomem<offset<0x074u>,       unsigned>;
            using I2S_NSOF_RST_N_SOFT              = iomem<offset<0x078u>,       unsigned>;
            using EDGE_DET_RST_N_SOFT              = iomem<offset<0x07Cu>,       unsigned>;
            using I2STX_FIFO_0_RST_N_SOFT          = iomem<offset<0x080u>,       unsigned>;
            using I2STX_IF_0_RST_N_SOFT            = iomem<offset<0x084u>,       unsigned>;
            using I2STX_FIFO_1_RST_N_SOFT          = iomem<offset<0x088u>,       unsigned>;
            using I2STX_IF_1_RST_N_SOFT            = iomem<offset<0x08Cu>,       unsigned>;
            using I2SRX_FIFO_0_RST_N_SOFT          = iomem<offset<0x090u>,       unsigned>;
            using I2SRX_IF_0_RST_N_SOFT            = iomem<offset<0x094u>,       unsigned>;
            using I2SRX_FIFO_1_RST_N_SOFT          = iomem<offset<0x098u>,       unsigned>;
            using I2SRX_IF_1_RST_N_SOFT            = iomem<offset<0x09Cu>,       unsigned>;
            using LCD_PNRES_SOFT                   = iomem<offset<0x0B4u>,       unsigned>;
            using SPI_PNRES_APB_SOFT               = iomem<offset<0x0B8u>,       unsigned>;
            using SPI_PNRES_IP_SOFT                = iomem<offset<0x0BCu>,       unsigned>;
            using DMA_PNRES_SOFT                   = iomem<offset<0x0C0u>,       unsigned>;
            using NANDFLASH_CTRL_ECC_RESET_N_SOFT  = iomem<offset<0x0C4u>,       unsigned>;
            using NANDFLASH_CTRL_NAND_RESET_N_SOFT = iomem<offset<0x0CCu>,       unsigned>;
            using SD_MMC_PNRES_SOFT                = iomem<offset<0x0D4u>,       unsigned>;
            using SD_MMC_NRES_CCLK_IN_SOFT         = iomem<offset<0x0D8u>,       unsigned>;
            using USB_OTG_AHB_RST_N_SOFT           = iomem<offset<0x0DCu>,       unsigned>;
            using RED_CTL_RESET_N_SOFT             = iomem<offset<0x0E0u>,       unsigned>;
            using AHB_MPMC_RESET_N_SOFT            = iomem<offset<0x0E4u>,       unsigned>;
            using AHB_MPMC_REFRESH_RESET_N_SOFT    = iomem<offset<0x0E8u>,       unsigned>;
            using INTC_RESET_N_SOFT                = iomem<offset<0x0ECu>,       unsigned>;

            using HP0_FIN_SELECT                   = iomem<offset<0x0F0u>,       unsigned>;
            using HP0_MDEC                         = iomem<offset<0x0F4u>,       unsigned>;
            using HP0_NDEC                         = iomem<offset<0x0F8u>,       unsigned>;
            using HP0_PDEC                         = iomem<offset<0x0FCu>,       unsigned>;
            using HP0_MODE                         = iomem<offset<0x100u>,       unsigned>;
            using HP0_STATUS                       = iomem<offset<0x104u>, const hp_status_t>;
            using HP0_ACK                          = iomem<offset<0x108u>, const unsigned>;
            using HP0_REQ                          = iomem<offset<0x10Cu>,       unsigned>;
            using HP0_INSELR                       = iomem<offset<0x110u>,       unsigned>;
            using HP0_INSELI                       = iomem<offset<0x114u>,       unsigned>;
            using HP0_INSELP                       = iomem<offset<0x118u>,       unsigned>;
            using HP0_SELR                         = iomem<offset<0x11Cu>,       unsigned>;
            using HP0_SELI                         = iomem<offset<0x120u>,       unsigned>;
            using HP0_SELP                         = iomem<offset<0x124u>,       unsigned>;

            using HP1_FIN_SELECT                   = iomem<offset<0x128u>,       unsigned>;
            using HP1_MDEC                         = iomem<offset<0x12Cu>,       unsigned>;
            using HP1_NDEC                         = iomem<offset<0x130u>,       unsigned>;
            using HP1_PDEC                         = iomem<offset<0x134u>,       unsigned>;
            using HP1_MODE                         = iomem<offset<0x138u>,       unsigned>;
            using HP1_STATUS                       = iomem<offset<0x13Cu>, const hp_status_t>;
            using HP1_ACK                          = iomem<offset<0x140u>, const unsigned>;
            using HP1_REQ                          = iomem<offset<0x144u>,       unsigned>;
            using HP1_INSELR                       = iomem<offset<0x148u>,       unsigned>;
            using HP1_INSELI                       = iomem<offset<0x14Cu>,       unsigned>;
            using HP1_INSELP                       = iomem<offset<0x150u>,       unsigned>;
            using HP1_SELR                         = iomem<offset<0x154u>,       unsigned>;
            using HP1_SELI                         = iomem<offset<0x158u>,       unsigned>;
            using HP1_SELP                         = iomem<offset<0x15Cu>,       unsigned>;
        };

    };

} // namespace dtl
