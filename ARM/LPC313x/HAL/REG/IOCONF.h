#pragma once

#include "dtl/common/iomem.h"

namespace dtl {

    struct IOCONF: address<0x1300'3000u> {

        struct GPIO: offset<0x1C0u> {
            using PIN       = iomem<offset<0x000u>, unsigned>;
            using MODE0     = iomem<offset<0x010u>, unsigned>;
            using MODE0_SET = iomem<offset<0x014u>, unsigned>;
            using MODE0_CLR = iomem<offset<0x018u>, unsigned>;
            using MODE1     = iomem<offset<0x020u>, unsigned>;
            using MODE1_SET = iomem<offset<0x024u>, unsigned>;
            using MODE1_CLR = iomem<offset<0x028u>, unsigned>;
        };

    };

} // namespace dtl
